#!/usr/bin/env python3

debug = True

### Imports for code modules ###

# Fix version check warning
try:
    from gi import require_version as girv
    girv('Gtk', '3.0')
    girv('Gdk', '3.0')
except:
    print('Could not find required Gtk/Gdk version')


# Check for Cairo support in PyGObject
try:
    from gi import require_foreign as girf
    girf('cairo')
except ImportError:
    print('Could not find Cairo support')

# Import libraries
from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import GLib
import sys
import cairo



### Import for plugin modules ###

# Import Cairo objects
import plugins as p



### Main code ###

def printd(string):
    global debug
    if debug:
        print('debug: ' + string)

# Each widget gets a WidgetInstance
class WidgetInstance:

    def __init__(self, widget, placement, size=(1,1), name='New Widget'):
        self.widget = widget
        self.placement = placement
        self.size = size
        self.name = name

    def tick(self):
        self.widget.tick()

# Coaster window and layout is defined here
class CoasterWindow(Gtk.ApplicationWindow):

    def __init__(self, app):
        Gtk.Window.__init__(self, title="Coaster", application=app)
        self.set_default_size(300, 300)
        self.set_decorated(False)

        # List to keep track of all widgets
        self.widget_list = []

        # Transparent background
        screen = self.get_screen()
        visual = screen.get_rgba_visual()
        if visual and screen.is_composited():
            self.set_visual(visual)

        self.set_app_paintable(True)
        self.show_all()

        def draw(self, widget, context):
            context.set_source_rgba(0, 0, 0, 0)
            context.set_operator(cairo.OPERATOR_SOURCE)
            context.paint()
            context.set_operator(cairo.OPERATOR_OVER)

        # Make sure we're maximized
        self.maximize()


        ### Widgets section ###
        # Add a clock
        #self.clock = p.ClockSimple()
        self.widget_list.append(WidgetInstance(p.ClockSimple(), (0,0), (1,1), 'Clock'))
        # Add a calendar
        self.calendar = Gtk.Calendar()


        ### Layout section ###
        # Put widgets into a grid
        self.grid = Gtk.Grid()
        self.grid.set_row_spacing(10)
        self.grid.set_column_spacing(10)
        self.grid.attach(self.widget_list[0].widget, 0, 0, 1, 1)
        self.grid.attach(self.calendar, 0, 1, 1, 1)


        # Put the grid in the main window
        self.add(self.grid)

    # Update dynamic widgets
    def tick(self):
        for widget_instance in self.widget_list:
            widget_instance.tick()
            printd('Calling tick() for widget \"' + widget_instance.name + '\"')
        return True

# Boilerplate code
class CoasterApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = CoasterWindow(self)
        win.show_all()
        GLib.timeout_add_seconds(1, win.tick)

    def do_startup(self):
        Gtk.Application.do_startup(self)

def main():
    app = CoasterApplication()
    exit_status = app.run(sys.argv)
    sys.exit(exit_status)

if __name__ == '__main__':
    main()

