debug = True

# Import libraries
from datetime import datetime
from gi.repository import Gtk, GObject

def printd(string):
    global debug
    if debug:
        print('debug: ' + string)

def square(ctx):
    ctx.move_to(0, 0)
    ctx.rel_line_to(2 * SIZE, 0)
    ctx.rel_line_to(0, 2 * SIZE)
    ctx.rel_line_to(-2 * SIZE, 0)
    ctx.close_path()

# Adapted from example at https://gist.github.com/jampola/473e963cff3d4ae96707
class ClockSimple(Gtk.Box):
    def __init__(self):
        Gtk.Box.__init__(self, spacing=10)

        self.label = Gtk.Label()
        self.pack_start(self.label, True, True, 0)

        #self.startclocktimer()

    def displayclock(self):
        datetimenow = str(datetime.now())
        self.label.set_label(datetimenow)
        return True

    def tick(self):
        self.displayclock()

    def startclocktimer(self):
        GObject.timeout_add_seconds(1, self.displayclock)


